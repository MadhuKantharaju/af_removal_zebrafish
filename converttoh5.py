import os
from skimage import io
import h5py
from tqdm import tqdm
import subprocess
from pathlib import Path

root_dir = '/home/mnagath/Documents/data/AF/af3/'
h5_files = '/home/mnagath/Documents/data/AF/af2/'
# please edit the path to the directory
image_list = [f for f in os.listdir(root_dir) if f.endswith(".tif")]


def create_h5(image_list, root_dir):
    for im_no in tqdm(range(len(image_list))):
        file_name = os.path.basename(image_list[im_no]).split('.')[0]
        file_name_split = file_name.split('_')
        if file_name_split[2] == '5dpi':
            img_read = io.imread(os.path.join(root_dir, image_list[im_no]))
            temp_data = h5py.File(h5_files + file_name + '.h5', 'w')
            temp_data['data'] = img_read
            temp_data.close()

create_h5(image_list, root_dir)

# ilastik_dir = subprocess.run(["/home/mnagath/Desktop/ilastik-1.3.3post3-Linux/run_ilastik.sh",
#                               '--headless', '--project="/home/mnagath/Desktop/ilastik-1.3.3post3-Linux/slices_class.ilp"',
#                               '--export_source:"Simple Segmentation"', '"/home/mnagath/Documents/data/AF/af2/*"'])
# print("`cd ~` ran with exit code %s" % ilastik_dir)


